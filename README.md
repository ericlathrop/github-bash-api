# github-bash-api

Bash scripts for querying the [GitHub REST API](https://docs.github.com/en/rest?apiVersion=2022-11-28)

By default the scripts will perform unauthenticated API access. If you want to authenticate
(for higher rate limits, etc.), then specify a [personal access token](https://github.com/settings/tokens?type=beta) with the `GITHUB_TOKEN` environment variable.

## Dependencies

Requires `curl` and [`jq`](https://github.com/jqlang/jq). If `jq` isn't
installed, it will run a version from docker.

## Commands

### `github-api /some/path`

Queries the GitHub REST API with the given path.

### `github-latest-release owner/repo`

Get the name of the tag for the repository's latest GitHub release.

### `github-latest-tag owner/repo`

Get the name of the latest tag for the repository. Tags are sorted by semver to
determine the latest version.
